/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.rotbot;

/**
 *
 * @author acer
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap Map;

    public Robot(int x, int y, char symbol, TableMap Map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.Map = Map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (Walk_N()) {
                    return false;
                }
                break;
            case 'S':
            case 's':
                if (Walk_S()) {
                    return false;
                }
                break;
            case 'E':
            case 'd':
                if (Walk_E()) {
                    return false;
                }
                break;
            case 'W':
            case 'a':
                if (Walk_W()) {
                    return false;
                }
                break;
            default:
                return false;
        }
        if (Map.isBomb(x, y)) {
            System.out.println("(Founded Bomb !!!" + x + ", " + y + ") ");
        }
        return true;
    }

    private boolean Walk_W() {
        if (Map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean Walk_E() {
        if (Map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean Walk_S() {
        if (Map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean Walk_N() {
        if (Map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
